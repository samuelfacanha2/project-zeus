# Project Zeus

Repositório de aplicação fullstack utilizando node, express, mongoDB, react e react native.

Projeto para implementação de um sistema de visualização de gastos com um backend integrando database(mongoDB hosteado online) e API, front end para melhor visualização e controle dos dados, e aplicativo mobile para inserção e visualização simples dos dados.

Estrutura do DB: {
    preco: Number,
    peso: Number,
    dia: Number,
    mes: Number,
    ano: Number,
    status: String
}

Feito

    BackEnd:
    - integração com DB (MongoDB hosteado online)
    - http requests para interface de dados via API

    FrontEnd:
    - pagina de visualização, com todas as compras de comida
    - página de "lixeira" para remoção definitiva do banco de dados
    - página de inserção de dados.
    - interface com API para ler e deletar dados

    Aplicativo Mobile:
    - interface simples de inserção de dados.

À Fazer
    FrontEnd:
    - Melhorar Visualização dos Dados

    Aplicativo Mobile:
    - Interface de visualização de dados
    - Melhorar design da inserção
