const mongoose = require('mongoose');
require('dotenv/config')
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true, useUnifiedTopology: true},()=>{console.log("connected to db.")});

const foodSchema = new mongoose.Schema({
    preco: Number,
    peso: Number,
    dia: Number,
    mes: Number,
    ano: Number,
    status: String
}, { collection: 'dogfood' }
);

module.exports = { Mongoose: mongoose, FoodSchema: foodSchema }