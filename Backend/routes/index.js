const express = require('express');
const { FoodSchema } = require('../db');
const router = express.Router();
const db = require("../db");
const Food = db.Mongoose.model('dogfood', db.FoodSchema, 'dogfood');


/* GET home page. */
router.get('/',(req, res, next) => {
  res.render('index', { title: 'Express' });
});

/* GET foodlist page. */
// router.get('/foodlist',(req, res)=>{
//   Food.find({}).lean().exec(
//      function (e, docs) {
//         res.render('foodlist', { "foodlist": docs });
//   });
// });
/* GET New Food page. */
router.get('/newfood',(req, res)=>{
  res.render('newfood', { title: 'Add New Food' });
  });

/* POST to Add Food Service */
// router.post('/addfood',(req, res)=>{

//   const foodPreco = req.body.preco;
//   const foodPeso = req.body.peso;
//   const foodMes = ((new Date(). getMonth())+1);
//   const foodAno = new Date().getFullYear();


//   const foodNew = new Food({ preco: foodPreco, peso: foodPeso, mes: foodMes, ano: foodAno, status:"ativo" });
//   foodNew.save(function (err) {
//       if (err) {
//           console.log("Error! " + err.message);
//           return err;
//       }
//       else {
//           console.log("Post saved");
//           res.redirect("foodlist");
//       }
//   });
// });

// A P I ------------------------------------------------------------------ A P I ------------------------------------------------------------------------ A P I --------------------

/*GET para API (todos)*/
router.get('/api/dogfood', async (req,res) =>{
  const food = Food.find({}).lean().exec(
    (err, data) => {
      try{
       res.json(data);
      }
      catch(err){
        console.log("Error! " + e.message);
          return e;
      }
 });
})

/*GET para API (específico)*/
router.get('/api/dogfood/single/:foodId', async (req,res) =>{

  const food = await Food.findById(req.params.foodId)
  res.json(food)

})

/*GET para API (mês/ano)*/
router.get('/api/dogfood/:year/:month/:isActive', async (req,res) =>{

  const food = await Food.find({ano:req.params.year,mes:req.params.month,status:req.params.isActive})
  res.json(food)

})

router.get('/api/dogfood/ativo/:isActive', async (req,res) =>{

  const food = await Food.find({status:req.params.isActive})
  res.json(food)

})

/*DELETE para API*/
router.delete('/api/dogfood/single/:foodId', async (req,res)=>{
  try{
  const removedFood = await Food.deleteOne({_id : req.params.foodId})
  res.json(removedFood)
  } catch(err){
    console.log("Error! " + e.message)
  }
})

/* post para api*/
router.post('/api/dogfood/addfood',(req, res)=>{

  const foodPreco = req.body.preco;
  const foodPeso = req.body.peso;
  const foodDia = req.body.dia;
  const foodMes = req.body.mes;
  const foodAno = req.body.ano;


  const foodNew = new Food({ preco: foodPreco, peso: foodPeso, dia:foodDia, mes: foodMes, ano: foodAno, status:"ativo" });
  foodNew.save(function (err) {
      if (err) {
          console.log("Error! " + err.message);
          return err;
      }
      else {
          console.log("Post saved");
      }
  });
});

router.put('/api/dogfood/single/:foodId',(req,res)=>{
  
    Food.findByIdAndUpdate({_id:req.params.foodId},req.body).then(()=>{
      Food.findOne({_id:req.params.foodId}).then((patchedFood)=>{
        res.send(patchedFood)
      })
      
    })

  //   patchedFood.save(function (err) {
  //     if (err) {
  //         console.log("Error! " + err.message);
  //         return err;
  //     }
  //     else {
  //         console.log("Post saved");
  //         res.json(patchedFood)
  //     }
  // })
})
module.exports = router;
