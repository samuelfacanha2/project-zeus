import React, {Component} from 'react'
import axios from 'axios'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'


export default class Inserir  extends Component {

  constructor(props) {
    super(props);
    this.state = { preco:'',
    peso:'',
    dia:'',
    mes:'',
    ano:'' };
  }

  
  precoHandler = (event) => {
    this.setState({preco: event.target.value});
  }
  pesoHandler = (event) => {
    this.setState({peso: event.target.value});
  }
  diaHandler = (event) => {
    this.setState({dia: event.target.value});
  }
  mesHandler = (event) => {
    this.setState({mes: event.target.value});
  }
  anoHandler = (event) => {
    this.setState({ano: event.target.value});
  }
  buttonHandler = (event) => {
    
    const res = axios.post(`http://localhost:3000/api/dogfood/addfood`,this.state).then((response) => {console.log(response)}, (error) => {console.log(error)});
    alert("Inserido com Sucesso!")
    window.location.reload();
  }

  render() {
      return (
        <div>
        <Head>
          <title>Inserir</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
  
        <div className="container">
          <h1 className="text-center text-6xl"><span className="text-blue-600">Inserir</span></h1>
          <h2 className="text-center text-4xl mt-8">Preencha os campos abaixo para <span className="text-blue-600">Inserir</span> uma compra:</h2>
          <div className="grid">
              <form className="card" id="insertCard">
                  <p>Preco(R$):<input type="text" name="preco" onChange={this.precoHandler} /></p>
                  <p>Peso(gramas):<input type="text" name="peso" onChange={this.pesoHandler} /></p>
                  <p>Dia:<input type="text" name="dia" onChange={this.diaHandler} /></p>
                  <p>Mes:<input type="text" name="mes" onChange={this.mesHandler} /></p>
                  <p>Ano:<input type="text" name="ano" onChange={this.anoHandler} /></p>
                  <br/>
                  <button type="button" className="text-xl bg-blue-50" onClick={this.buttonHandler}>Salvar</button>
              </form>
          </div>
          <br/>
          <div className="text-xl text-center text-blue-400"><Link href="/">Home</Link></div>
        </div>
      </div>
      )
  }
}

