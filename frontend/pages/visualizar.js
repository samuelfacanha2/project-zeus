import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import Dashboard from '../components/Dashboard'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Visualizar</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="container">
        <h1 className="text-center text-6xl"><span className="text-blue-600">Compras:</span></h1>
        <div className="grid">
            <Dashboard url={`http://localhost:3000/api/dogfood/ativo/ativo`}  />
        </div>

      </div>
    </div>
  )
}
