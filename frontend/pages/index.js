import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="container">
        <h1 className="text-center text-6xl"> Olá, <span className="text-blue-600">Felipe</span></h1>
        <h2 className="text-center text-4xl mt-8">O que deseja fazer?</h2>
        <div id="GridM">
          <Link href='/inserir'>
              <a className="card">
                <h3>Inserir</h3>
                <p>Inserir uma compra de ração</p>
              </a>
            </Link>
            <Link href="/visualizar"> 
              <a className="card">
                <h3>Visualizar</h3>
                <p>Visualizar as compras</p>
              </a>
            </Link>
            <Link href="/remover"> 
              <a className="card">
                <h3>Lixeira</h3>
                <p>Excluir compras inativas do database</p>
              </a>
            </Link>
        </div>
      </div>
    </div>
  )
}
