import 'tailwindcss/tailwind.css'
import '../styles/containers.css'
import '../styles/specific.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
