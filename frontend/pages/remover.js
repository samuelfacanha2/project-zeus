import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import RemoverDashboard from '../components/RemoverDashboard'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Remover</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="container">
        <h1 className="text-center text-6xl"><span className="text-blue-600">Compras Inativas:</span></h1>
        <div className="grid">
            <RemoverDashboard url={`http://localhost:3000/api/dogfood/ativo/inativo`}  />
        </div>

      </div>
    </div>
  )
}