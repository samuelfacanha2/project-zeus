import React, { Component } from 'react'
import Link from 'next/link'
import axios from 'axios';

export default class RemoverCard extends Component {


    constructor(props) {
        super(props);
        this.state = {
            id:this.props.id,
            preco:this.props.preco,
            peso:this.props.peso,
            mes:this.props.mes,
            dia:this.props.dia,
            ano:this.props.ano
        };
      }
    
    componentDidMount(){
        //console.log(this.props)
        //this.setState(this.props)
        //console.log(this.state.id)
    }

    inactivateHandler(event){
        
    }

    render() {
        
        return (
                <div className="pcard">
                
                
                    <span className='px-16 py-3 text-3xl bg-blue-50'>preço: R$ {this.state.preco}</span>
                    <span className='px-16 py-3 text-3xl border border-gray-100'>peso: {this.state.peso}g </span>
                    <span className='px-16 py-3 text-3xl bg-blue-50'>data: {this.state.dia}/{this.state.mes}/{this.state.ano} </span>
                    
                    <h3>

                    <br/>
                    <span className="text-gray-400">id: {this.state.id} </span>
                    <button type="button" className="text-xl2 font-bold text-red-300 bg-gray-200 mt-5" onClick={()=>{
                        console.log(this.state.id)
                        axios.delete(`http://localhost:3000/api/dogfood/single/${this.state.id}`)
                        window.location.reload()
                    }}>Deletar</button>
                    <span> </span>
                    <button type="button" className="text-xl2 font-bold text-green-300 bg-gray-200" onClick={()=>{
                        console.log(this.state.id)
                        axios.put(`http://localhost:3000/api/dogfood/single/${this.state.id}`,{status:"ativo"})
                        window.location.reload()
                    }}>Reativar</button>
                    
                    </h3>

                </div>
        )
    }
}
