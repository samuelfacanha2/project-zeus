import React, { Component } from 'react'
import axios from 'axios';
import CompraCard from './CompraCard'
import Link from 'next/link'

export default class Dashboard  extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            url:this.props.url,
            compra: null,
            dia:'',
            mes:'',
            ano:'',
            totalPreco:0,
            totalPeso:0 };
      }

    
    async componentDidMount(){

        const res = await axios.get(this.state.url);
        this.setState({compra: res.data});

        this.state.compra.forEach(c=> {
            this.state.totalPreco += parseFloat(c.preco)
            this.state.totalPeso += parseInt(c.peso)
        });
        this.forceUpdate();
    }
    
    mesHandler = (event) => {
        this.setState({mes: event.target.value});
    }
    anoHandler = (event) => {
        this.setState({ano: event.target.value});
        console.log(this.state)
    }
    buttonHandler = async (event) => {

        this.setState({totalPreco:0, totalPeso:0})
        this.setState({url:`http://localhost:3000/api/dogfood/${this.state.ano}/${this.state.mes}/ativo`},async ()=>{
            console.log(`http://localhost:3000/api/dogfood/${this.state.ano}/${this.state.mes}/ativo`)
            console.log("new url:", this.state.url)
    
            const res = await axios.get(this.state.url);
            this.setState({compra: res.data});

            this.state.compra.forEach(c=> {
                this.state.totalPreco += parseFloat(c.preco)
                this.state.totalPeso += parseInt(c.peso)
            });
            this.forceUpdate();
        })
        

      }

    render() {
        return (
            <div className="dashboard">
                <div className="text-xl text-blue-400"><Link href="/">Home</Link></div>
                
                <form className="card">
                <h4 className="text-xl">Mes: <input type="text" name="mes" onChange={this.mesHandler} /> Ano: <input type="text" name="ano" onChange={this.anoHandler} /></h4>
                <br/>
                <button type="button" className="text-xl bg-blue-100" onClick={this.state.mes&&this.state.ano? this.buttonHandler : ()=>{alert("Insira um mês e um ano")}}>Buscar</button>
                <span className="text-xl ml-72"> Total: R${this.state.totalPreco}, {this.state.totalPeso}g</span>
                </form>

                {this.state.compra ? (
                    <div id="GridG">
                        {this.state.compra.map(compra =>(
                            <CompraCard
                            key= {compra._id}
                            preco = {compra.preco}
                            peso = {compra.peso}
                            id = {compra._id}
                            dia = {compra.dia}
                            mes = {compra.mes}
                            ano = {compra.ano}

                            />
                        ))}
                    </div>
                )   : ( <h1> Carregando... </h1>)
                }
            </div>
        )
    }
}

