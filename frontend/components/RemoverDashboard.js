import React, { Component } from 'react'
import axios from 'axios';
import RemoverCard from './RemoverCard'
import Link from 'next/link'

export default class Dashboard  extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            url:this.props.url,
            compra: null,
            dia:'',
            mes:'',
            ano:''};
      }

    
    async componentDidMount(){
        
        const res = await axios.get(this.state.url);
        this.setState({compra: res.data});
    }
    mesHandler = (event) => {
        this.setState({mes: event.target.value});
    }
    anoHandler = (event) => {
        this.setState({ano: event.target.value});
        console.log(this.state)
    }
    buttonHandler = async (event) => {

        this.setState({url:`http://localhost:3000/api/dogfood/${this.state.ano}/${this.state.mes}/inativo`},async ()=>{
            console.log(`http://localhost:3000/api/dogfood/${this.state.ano}/${this.state.mes}/inativo`)
            console.log("new url:", this.state.url)
    
            const res = await axios.get(this.state.url);
            this.setState({compra: res.data});
            console.log(this.state)
            this.forceUpdate();
        })
        

      }

    render() {
        return (
            <div className="dashboard">
                <div className="text-xl text-blue-400"><Link href="/">Home</Link></div>
                
                <form className="card">
                <h4 className="text-xl">Mes: <input type="text" name="mes" onChange={this.mesHandler} /> Ano: <input type="text" name="ano" onChange={this.anoHandler} /></h4>
                <br/>
                <button type="button" className="text-xl bg-blue-100" onClick={this.buttonHandler}>Buscar</button>
              </form>

                {this.state.compra ? (
                    <div id="GridG">
                        {this.state.compra.map(compra =>(
                            <RemoverCard
                            key= {compra._id}
                            preco = {compra.preco}
                            peso = {compra.peso}
                            id = {compra._id}
                            dia = {compra.dia}
                            mes = {compra.mes}
                            ano = {compra.ano}
                            />
                        ))}
                    </div>
                )   : ( <h1> Carregando... </h1>)
                }
            </div>
        )
    }
}

